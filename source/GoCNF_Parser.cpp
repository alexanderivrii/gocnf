
#include <iostream>
#include <assert.h>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <sstream>
#include <string>

#include "CNF.h"
#include "GoCNF.h"

using std::sort;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::fstream;
using std::string;
using std::stringstream;
using std::getline;
using std::abs;

typedef vector<int>::const_iterator c_iter;

void removeTabs(string& string){
	if(string.empty()){
		return;
	}
	unsigned i = 0;
	unsigned len = string.length();
	while(i < len){
		if(string.at(i) == '\t'){
			string.at(i) = ' ';
		}
		i++;
	}
}

void getWords(stringstream& stream, vector<string>& words){
	string word;
	while(!stream.eof()){
		getline(stream, word, ' ');
		if(!word.length() || !word[0]){
			continue;
		}
		words.push_back(word);
		word.clear();
	}
}

int stringToInt(const string& str){
	if(str.empty()){
		return 0;
	}
	if(str.length() == 1
		 && str[0] >= '0'
		 && str[0] <= '9'){
		return str[0] - '0';
	}
	int res = 0;
	bool isNegative(false);
	unsigned i = 0;

	if(str[0] == '-'){
		i++;
		isNegative = true;
	}
	if(str[0] == '+'){
		i++;
	}
	while(i < str.length()){
		if(str[i] == ';'){
			break;
		}
		if(str[i] < '0' || str[i] > '9') {
			return 0;
		}
		res *= 10;
		res += str[i] - '0';
		i++;
	}
	return isNegative ? -res : res;
}

bool openFile(const char *filename, fstream& input){
	if(!filename){
		cerr << "Go_CNF: Error - Empty string" << endl;
		return false;
	}

	input.open(filename);
	if(!input.is_open()){
		cerr << "Go_CNF: Error - Could not open " << filename << endl;
		return false;
	}
	return true;
}

bool openToWriteFile(const char *filename, fstream& output){
	if(!filename){
		cerr << "Go_CNF: Error - Empty string" << endl;
		return false;
	}
	output.open(filename, fstream::trunc | fstream::out);
	if(!output.is_open()){
		cerr << "Go_CNF: Error - Could not create " << filename
				<< " for writing output" << endl;
		return false;
	}
	return true;
}

char inline toUpper(char c){
	if(c >= 'a' && c <= 'z'){
		return c - 32;
	}
	return c;
}

int caseInsensitive(const string& str1,
	const string& str2){
	unsigned i = 0, j = 0;
	unsigned len1 = str1.length(), len2 = str2.length();
	char c1, c2;
	while(i < len1 && j < len2){
		c1 = toUpper(str1[i]);
		c2 = toUpper(str2[j]);;
		if(c1 > c2){
			return 1;
		}
		if(c1 < c2){
			return -1;
		}
		i++;
		j++;
	}
	if(len1 > len2){
		return 1;
	}
	if(len1 < len2){
		return -1;
	}
	return 0;
}

bool initializeLine(const vector<string>& words){
	if(words.empty()){
		return false;
	}
	const string& firstWord = words.at(0);
	if(firstWord.length() == 1 &&
	    (firstWord[0] == 'p' || firstWord[0] == 'P')){
		if(words.size() >= 4 && words.at(1).length() == 3
			&& (caseInsensitive(words.at(1), "CNF") == 0)){
			return true;
		}
	}
	return false;
}

bool commentLine(const vector<string>& words){
	if(words.empty()){
		return true;
	}
	const string& firstWord = words.at(0);
	if(firstWord.length() == 1 &&
		 (firstWord[0] == 'c' || firstWord[0] == 'C')){
		return true;
	}
	return false;
}

bool checkParams(const vector<string>& words,
		int& variables, int& clauses){
	if(commentLine(words)){
		return false;
	}
	if(initializeLine(words)){
		variables = stringToInt(words.at(2));
		if(variables <= 0){
			variables = 0;
			return false;
		}
		clauses = stringToInt(words.at(3));
		if(clauses <= 0){
			variables = 0;
			clauses = 0;
			return false;
		}
		return true;
	}
	return false;
}

bool initialize(fstream& input, int& variables, int& clauses){
	string line;
	stringstream line_stream;
	vector<string> words;
	while(!input.eof()){
		line_stream.clear();
		words.clear();
		getline(input, line);
		if(line.empty()){
			continue;
		}
		removeTabs(line);
		line_stream << line;
		getWords(line_stream, words);
		if(checkParams(words, variables, clauses)){
			return true;
		}
	}
	return false;
}


void reportOriginalVar(VariableManager& variableManager, unsigned newVar) {
    if (newVar > variableManager.getMaxVar()) {
        variableManager.setMaxOriginalVar(newVar);
    }
}

void addClause(VariableManager& variableManager, CNF &cnf,
        const vector<string>& words){
	if(commentLine(words)){
		return ;
	}
	vector<int> lits;
	for(vector<string>::const_iterator iter = words.begin();
			iter != words.end(); ++iter){
		const string& word = *iter;
		int temp_var =  stringToInt(word);
		if(temp_var == 0){
			break;
		}
        reportOriginalVar(variableManager, abs(temp_var));
		lits.push_back(temp_var);
	}
	if(lits.empty()){
		return;
	}
	cnf.addClause(lits);
}

void addClauses(fstream& input, VariableManager& variableManager, CNF &cnf) {
	string line;
	stringstream line_stream;
	vector<string> words;
	while(!input.eof()){
		line_stream.clear();
		words.clear();
		getline(input, line);
		if(line.empty()){
			continue;
		}
		removeTabs(line);
		line_stream << line;
		getWords(line_stream, words);
		addClause(variableManager, cnf, words);
	}
}

void printMessages(int max_variable, int clauses_found,
		int variables, int clauses) {
	if(clauses_found == 0){
		cerr << "Go_CNF: Warning - No clauses found, expected: " <<
			clauses << endl;
		return;
	}
	cout << "Go_CNF: Added " << clauses_found << " clauses" << endl;
	if(clauses_found != clauses) {
		cerr << "Go_CNF: Warning - Added: " <<  clauses_found
		  << " clauses, expected: " << clauses << endl;
	}
	if(max_variable > variables){
		cerr << "Go_CNF: Warning - A variable with index: " << max_variable
		   << " added, bigger than max variable specified: " << variables << endl;
	}
}


void GoCNF::readFromFileDIMACS(VariableManager& variableManager,
		CNF &cnf, const char* const filename){
	fstream input;
	if(!openFile(filename, input)){
 		return;
	}

	int declaredVars = 0, declaredClauses = 0;
	if(!initialize(input, declaredVars, declaredClauses)){
		cerr << "Go_CNF: Error - No initialize line found at \"" <<
			filename << "\"" << endl;
		input.close();
		return;
	}

	addClauses(input, variableManager, cnf);
    unsigned actualVars = variableManager.getMaxVar(), actualClauses = cnf.constraints.size();
	printMessages(actualVars, actualClauses, declaredVars, declaredClauses);
	input.close();
}

bool commentLineOBP(const vector<string>& words){
	if(words.empty()){
		return true;
	}
	const string& firstWord = words.at(0);
	if(firstWord.length() == 1 &&
		 firstWord[0] == '*'){
		return true;
	}
	return false;
}

// Adding support for ~ (negation of literals)
int getVarOBP(const string& word){
	if(word.length() <= 1) {
		return 0;
	}
    if (word[0] == '~') {
        if (word.size() <= 2) return 0;
        if (word[1] != 'x') return 0;
        int var = stringToInt(word.substr(2));
        return -var;
    }
	if(word[0] != 'x'){
		return 0;
	}
	return stringToInt(word.substr(1));
}

// Checks if word is of the form "15:" or "-17".
int getActVarOBP(const std::string& word)
{
    unsigned length = word.length();
    if ((length>=2) && (word.substr(length-1, length) == ":")) {
        return stringToInt(word.substr(0, length-1));
    }
    return 0;
}

void printErrorLine(){
	cerr <<  "Go_CNF: Warning - Undefined line encountered" << endl;
}

void printAddedContradiction(){
	cerr <<  "Go_CNF: Warning - Added a contradiction" << endl;
}
void addContradiction(CNF &cnf, int var){
	assert(var != 0);
	cnf.addUnaryClause(var);
	cnf.addUnaryClause(-var);
}

void addClauseOBP(VariableManager& variableManager,
		CNF &cnf, const vector<string>& words) {
	if(commentLineOBP(words)){
		return ;
	}

	vector<int> lits, activationLits;
    int act;
	ConstraintType type;
	int var_coeff;
	int negative_vars = 0;
	vector<string>::const_iterator str = words.begin();
	for(; str != words.end(); ++str) {        
		if(caseInsensitive(*str, "<=") == 0){
			type = LEQ_CONSTRAINT;
			break;
		}
		if(caseInsensitive(*str, ">=") == 0){
			type = GEQ_CONSTRAINT;
			break;
		}
		/*   we support '=' using this method:
		 *   We add two equivalent constrains, one with '<=' and the other '>='
         *   Comment: this is a good solution.
         *   Another (more complicated) possibility is to extend the possible type of constraint to include '=',
         *   as there are some encodings that can handle those directly.
		 * */
		if(caseInsensitive(*str, "=") == 0 ||
				caseInsensitive(*str, "==") == 0){
			type = EQ_CONSTRAINT;
			break;
		}
        
        act = getActVarOBP(*str);
        if (act != 0) {
            activationLits.push_back(act);
            reportOriginalVar(variableManager, abs(act));
            continue;
        }

		var_coeff = stringToInt(*str);
		++str;
		if(var_coeff == 0 || str == words.end()){
			printErrorLine();
			return;
		}
		int var_index = getVarOBP(*str);
		if(var_index == 0){
			printErrorLine();
			return;
		}
        reportOriginalVar(variableManager, abs(var_index));
		if ((var_coeff > 0) && (var_index > 0)) {
            // +1 x14
            lits.push_back(var_index);
        }
        else if ((var_coeff > 0) && (var_index < 0)) {
            // +1 ~x14
            lits.push_back(var_index);
        }
		else if ((var_coeff < 0) && (var_index > 0)) {
            // -1 x14
            lits.push_back(-var_index);
            negative_vars++;
        }
        else if ((var_coeff < 0) && (var_index < 0)) {
            // -1 ~x14
            lits.push_back(-var_index);
            negative_vars++;
        }
	}
	if(lits.empty()){
		printErrorLine();
		return;
	}

	++str;
	if(str == words.end()){
		printErrorLine();
		return;
	}
	int k = stringToInt(*str);
    cnf.setActivationLits(activationLits);
    k = k + negative_vars;
    if(type == EQ_CONSTRAINT){
    	// we add two equivalent constraint
    	// if we get x1 + ... + xn = 0,
    	// we add the trivial >= 0 constraint
        cnf.addConstraint(Constraint(GEQ_CONSTRAINT, lits, k < 0 ? 0 : k));
        for (unsigned i=0; i<lits.size(); ++i){
        	lits[i] *= -1;
        }
        k = lits.size()-k;
        // where n < k, we add a clause that insures the CNF
        // cannot be satisfied by any assignment?
        // i.e. (x1 >= 1) && (~x1 => 1)
        // and print a warning: "A contradiction added to CNF"
        if(k < 0){
        	addContradiction(cnf, lits[0]);
        	printAddedContradiction();
        	return;
        }
        cnf.addConstraint(Constraint(LEQ_CONSTRAINT, lits, k));
        return;
    }
    
    if ((type == LEQ_CONSTRAINT) || (k == 1)) {
        // where n < k, we add a clause that insures the CNF
        // cannot be satisfied by any assignment?
        // i.e. (x1 >= 1) && (~x1 => 1)
        // and print a warning: "A contradiction added to CNF"
        if(k < 0){
        	addContradiction(cnf, lits[0]);
        	printAddedContradiction();
        	return;
        }
        // This is either an at-most-k constraint or a clause
        cnf.addConstraint(Constraint(type, lits, k));
    }
    else {
        // Unfortunately, our translation methods do not know how to handle this natively,
        // so we need to convert at-least to at-most
        for (unsigned i=0; i<lits.size(); ++i){
        	lits[i] *= -1;
        }
        k = lits.size()-k;
        // where n < k,  we add a clause that insures the CNF
        // cannot be satisfied by any assignment?
        // i.e. (x1 >= 1) && (~x1 => 1)
        // and print a warning: "A contradiction added to CNF"
        if(k < 0){
        	addContradiction(cnf, lits[0]);
        	printAddedContradiction();
        	return;
        }
        cnf.addConstraint(Constraint(LEQ_CONSTRAINT, lits, k));
    }
}

void addClausesOBP(fstream& input, VariableManager& variableManager, CNF &cnf){
	string line;
	stringstream line_stream;
	vector<string> words;
	while(!input.eof()){
		line_stream.clear();
		words.clear();
		getline(input, line);
		if(line.empty()){
			continue;
		}
		removeTabs(line);
		line_stream << line;
		getWords(line_stream, words);
		addClauseOBP(variableManager, cnf, words);
	}
}



void printMessagesOPB(int max_variable, int clauses_found){
	if(clauses_found == 0){
		cerr << "Go_CNF: Warning - No clauses found" << endl;
		return;
	}
	cout << "Original OPB: #variables = " <<  max_variable
			<< ", #clauses = " << clauses_found  << endl;
}


void GoCNF::readFromFileOPB(VariableManager &variableManager, CNF &cnf,
			const char* const filename){
	fstream input;

	if(!openFile(filename, input)){
 		return;
	}
	addClausesOBP(input, variableManager, cnf);
   	int max_variables = variableManager.getMaxVar(), clauses_found = cnf.constraints.size();
	printMessagesOPB(max_variables, clauses_found);
	input.close();
}

bool compareConstraints(const Constraint& a, const Constraint& b){
	if(a.getLits().size() < b.getLits().size()){
		return false;
	}
	return true;
}

void GoCNF::writeToFileDIMACS(VariableManager &variableManager, CNF &cnf,
		const char* const filename, bool sort){
	fstream output;
	if(!openToWriteFile(filename, output)){
 		return;
	}
	output << "p cnf " << variableManager.getMaxVar()
			<< " " << cnf.constraints.size() << endl;

	// sorting is disabled by default
	if(sort){
		std::sort(cnf.constraints.begin(),
				cnf.constraints.begin()+cnf.constraints.size(),
				compareConstraints);
	}
	for(vector<Constraint>::const_iterator iter = cnf.constraints.begin();
			iter != cnf.constraints.end(); ++iter){
		const Constraint& constraint = *iter;
		assert(constraint.getK() == 1
				&& constraint.getType() == GEQ_CONSTRAINT);

		for(c_iter iter = constraint.getActivationLits().begin();
				iter != constraint.getActivationLits().end(); ++iter){
			output << -(*iter) << " ";
		}                
		for(c_iter iter = constraint.getLits().begin();
				iter != constraint.getLits().end(); ++iter){
			output << *iter << " ";
		}
		output << "0" << endl;
	}
	output.close();
}

void addValues(fstream& input, vector<int>& values){
	string line;
	stringstream line_stream;
	vector<string> words;
	while(!input.eof()){
		line_stream.clear();
		words.clear();
		getline(input, line);
		if(line.empty()){
			continue;
		}
		removeTabs(line);
		line_stream << line;
		getWords(line_stream, words);
		if(words.size() == 0){
			return;
		}
		unsigned index = 0;
		while(index < words.size()){
			int value = stringToInt(words[index]);
			if(value == 0){
				return;
			}

			if(value > 0){
				values.push_back(1);
			} else {
				values.push_back(0);
			}
			index++;
		}

	}
}
bool GoCNF::readFromSAT(vector<int>& values, const char* filename){
	fstream input;
	if(!openFile(filename, input)){
 		return false;
	}
	string line;
	stringstream line_stream;
	vector<string> words;
	while(!input.eof()){
		line_stream.clear();
		words.clear();
		getline(input, line);
		if(line.empty()){
			continue;
		}
		removeTabs(line);
		line_stream << line;
		getWords(line_stream, words);
		if(words.size() < 1){
			return false;
		}
		if(caseInsensitive(words[0], "UNSAT") == 0){
			return false;
		}
		if(caseInsensitive(words[0], "SAT") == 0){
			addValues(input, values);
			return true;
		}
	}
	cerr << "Go_CNF: Warning - undefined SAT results file format" << endl;
	return false;
}



void printErrorNoAssignment(int literal){
	cerr << "Go_CNF: Error - Assignment for literal \"" << literal
			<< "\" was not provided" << endl;
}

bool GoCNF::validateAssignment(std::vector<int>& assignment, CNF &cnf){
	if(assignment.size() == 0){
		return false;
	}
	if(cnf.constraints.size() == 0){
		return true;
	}
	const int assignemnts_number = assignment.size();
	for(unsigned i = 0; i < cnf.constraints.size(); i++){
		const Constraint& constraint = cnf.constraints[i];
		if(constraint.getLits().size() == 0){
			continue;
		}
		bool clause_satisfied(false);
		const vector<int>& lits = constraint.getLits();
		for(unsigned j = 0; j < lits.size(); j++){
			assert(lits[j] != 0);
			if(lits[j] > assignemnts_number){
				printErrorNoAssignment(lits[j]);
				return false;
			}
			if(lits[j] > 0){
				if(assignment[lits[j] - 1] == 1){
					clause_satisfied = true;
					break;
				}
			} else {
				if(assignment[abs(lits[j]) - 1] == 0){
					clause_satisfied = true;
					break;
				}
			}
		}
		if(clause_satisfied){
			continue;
		}
		return false;
	}
	return true;
}

void GoCNF::excludeAssignment(const std::vector<int>& exclude, const std::vector<int>& assignment,
			VariableManager &variableManager, CNF &cnf){
	if(exclude.size() == 0){
		return;
	}

	vector<int> literals_negation;
	for(unsigned i = 0; i < exclude.size(); i++){
		unsigned excluded = abs(exclude[i]);
		if(!excluded){
			// if we see literal 0, we stop
			break;
		}
		if(excluded > assignment.size()){
			printErrorNoAssignment(excluded);
			return;
		}
		if(excluded > variableManager.getMaxVar()){
			variableManager.setMaxOriginalVar(excluded);
		}
		int negated_literal = assignment[excluded-1] == 0 ? excluded : -excluded;
		literals_negation.push_back(negated_literal);
	}
	if(literals_negation.size() == 0){
		return;
	}
	cnf.addClause(literals_negation);
}
