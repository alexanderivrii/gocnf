#ifndef _CNF_H
#define _CNF_H

#include <vector>

// At this point we consider two types of constraints:
//  GEQ: x1 + ... + xn >= k (also called at-least-k),
//  LEQ: x1 + ... + xn <= k (also-called at-most-k) 
// Note1: the usual CNF clause corresponds to constraint >=1
// Note2: There may be also value in considering other
// constraints such as k1 <= x1 + ... + xn <= k2;
// even though such a constraint can be easily formulated as
// two constraints above, shared common structure would be lost.

// Update: we now allow a set of activation literals per constraints,
// that is, we also support constrains of the type
//   a1 & ... & an -> (x1 + ... + xn >=k), and 
//   a1 & ... & an -> (x1 + ... + xn <=k)
// The activation literals are always stored separately, 
// and are used when outputing constraint in final CNF.

// Constraint types supported
enum ConstraintType 
{
    LEQ_CONSTRAINT, // x1 + ... + xn <= k
    GEQ_CONSTRAINT, // x1 + ... + xn >= k
	EQ_CONSTRAINT // x1 + ... + xn = k
};

// type holds the type of the constraint
// lits holds literals corresponding to {x1, ..., xn}
// k is the constant 
class Constraint
{
    ConstraintType   type;
    std::vector<int> lits;
    std::vector<int> acts;
    unsigned         k;
public:
    Constraint(ConstraintType cType, const std::vector<int>& cLits
    		, unsigned cK) : type(cType), lits(cLits), k(cK) { }
    Constraint(ConstraintType cType, const std::vector<int>& cLits, const std::vector<int>& cActivationLits, unsigned cK) :
            type(cType), lits(cLits), acts(cActivationLits), k(cK) { }
    Constraint(const Constraint& constraint) : type(constraint.type),
    		lits(constraint.lits), acts(constraint.acts), k(constraint.k){}
    ConstraintType getType() const { return type; }
    std::vector<int>& getLits() { return lits; }
    std::vector<int>& getActivationLits() { return acts; }
    const std::vector<int>& getLits() const { return lits; }
    const std::vector<int>& getActivationLits() const { return acts; }
    unsigned getK() const { return k; }
    void setActivationLits(const std::vector<int>& activationLits) { acts = activationLits; }
};

// holds all constraints
class CNF
{
public:
    std::vector<Constraint> constraints;
    
    // This is somewhat of a hack. Each constraint added using one of 'add' methods
    // inherits the current set of activation literals
    std::vector<int> tempActivationLits; 

    // add methods
    void addClause(const std::vector<int>& lits); 
    void addUnaryClause(int lit1);
    void addBinaryClause(int lit1, int lit2);
    void addTernaryClause(int lit1, int lit2, int lit3);
    void addQuaternaryClause(int lit1, int lit2, int lit3, int lit4);
    void addConstraint(const Constraint& c);

    // working with activation literals
    const std::vector<int>& getActivationLits() const {
    	return tempActivationLits;
    }
    void setActivationLits  (const std::vector<int>& activationLits) {
    	tempActivationLits = activationLits;
    }
    void clearActivationLits() {
    	tempActivationLits.clear();
    }
        
    // printing
    void printConstraint(const Constraint & c);
    void print(); 
};


#endif
