// Implements the sequential counter encoding
// Reference: Sinz

#include <iostream>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "CNF.h"
#include "GoCNF.h"

using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;

class var_helper {
    unsigned n, k;
    vector<unsigned> R;
    
public:
	var_helper(VariableManager &variableManager,
			unsigned n_in , unsigned k_in) : n(n_in), k(k_in) {
        //generating new helper variables
        for(unsigned i = 0; i < n; ++i) {
            for(unsigned j = 0; j < k; ++j) {
                R.push_back(variableManager.getNewVar());
            }
        }
    }

	unsigned get_var(unsigned i, unsigned j) {
        assert (i >= 1 && j >= 1);
        unsigned index = (i-1)*k + (j-1);
        assert (index < R.size());
        return R[index];
	}
};

// The Sequential Counter for creating a CNF for x1 + ... + xn <=k.
// Second implementation - by Carsten Sinz
void cnfizeAtMost_K_SequentialCounter_Sinz(const Constraint& constraint,
		VariableManager &variableManager, CNF &cnf){
	assert(constraint.getType() == LEQ_CONSTRAINT);

	unsigned k = constraint.getK();
    const std::vector<int>& x = constraint.getLits();
	unsigned n = x.size();


    // This is a 1-line command to pass all activation literals to translated constraints
    cnf.setActivationLits( constraint.getActivationLits() );

    var_helper s(variableManager, n , k);

	if(k == 1) {
		cnf.addBinaryClause(-x[0], s.get_var(1,1));
		cnf.addBinaryClause(-x[n-1], -s.get_var(n-1,1));
		for(unsigned i = 2; i <= n-1; ++i){
			cnf.addBinaryClause(-x[i-1], s.get_var(i,1));
			cnf.addBinaryClause(-s.get_var(i-1,1), s.get_var(i,1));
			cnf.addBinaryClause(-x[i-1], -s.get_var(i-1,1));
		}
		return;
	}
	assert(k > 1 && n >= 2);
	cnf.addBinaryClause(-x[0], s.get_var(1,1));
	for(unsigned j = 2; j <= k; ++j){
		cnf.addUnaryClause(-s.get_var(1,j));
	}
	for(unsigned i = 2; i <= n-1; ++i){
		cnf.addBinaryClause(-x[i-1], s.get_var(i,1));
		cnf.addBinaryClause(-s.get_var(i-1,1), s.get_var(i,1));
		for(unsigned j = 2; j <= k; ++j){
			cnf.addTernaryClause(-x[i-1],-s.get_var(i-1,j-1), s.get_var(i,j));
			cnf.addBinaryClause(-s.get_var(i-1,j), s.get_var(i,j));
		}
		cnf.addBinaryClause(-x[i-1], -s.get_var(i-1,k));
	}
	cnf.addBinaryClause(-x[n-1], -s.get_var(n-1,k));
	cnf.clearActivationLits();
}

