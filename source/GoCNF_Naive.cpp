// Implements the naive encoding

#include <iostream>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "CNF.h"
#include "GoCNF.h"

using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;

void addAllPermConstraints(const vector<int>& lits, unsigned left,
		unsigned idx, vector<int> &k_subset, CNF &cnf){
    if(left == 0) {
        cnf.addClause(k_subset);
        return;
    }
    if (lits.size() < left + idx) {
        return;
    }
    for(unsigned i = idx; i < lits.size(); i++){
        k_subset.push_back(-lits[i]);
        addAllPermConstraints(lits,left-1,i+1,k_subset,cnf);
        k_subset.pop_back();
    }
}

// The naive (also called pairwise) algorithm for creating a CNF for x1 + ... + xn <=k. 
//  iterate over all different subsets of size k+1 of variables, 
// and add a constraint stating that at least one of the variables in this subset should be false.
void cnfizeAtMost_K_Naive(const Constraint& constraint,
		VariableManager &variableManager, CNF &cnf )
{
    assert(constraint.getType() == LEQ_CONSTRAINT);

    // This is a 1-line command to pass all activation literals to translated constraints
    cnf.setActivationLits( constraint.getActivationLits() );

	vector<int> k_subset;
	addAllPermConstraints(constraint.getLits(), constraint.getK() + 1, 0, k_subset, cnf);

	cnf.clearActivationLits();
}

// The naive (also called pairwise) algorithm for creating a CNF for x1 + ... + xn <=1.
// Creates binary clauses (-xi, -xj) for i!=j. No auxiliary variables are necessary.
void cnfizeAtMostOne_Naive(const Constraint& constraint,
		VariableManager &variableManager, CNF &cnf)
{
    assert(constraint.getType() == LEQ_CONSTRAINT);
    assert(constraint.getK() == 1);
    // This is a 1-line command to pass all activation literals to translated constraints
    cnf.setActivationLits( constraint.getActivationLits() );
    const std::vector<int>& lits = constraint.getLits();
    for (unsigned i=0; i<lits.size(); ++i) {
        for (unsigned j=i+1; j<lits.size(); ++j) {
		cnf.addBinaryClause(-lits[i], -lits[j]);
        }
    }

	cnf.clearActivationLits();
}

