#include <iostream>
#include <assert.h>
#include <cmath>
#include <vector>


#include "GoCNF.h"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;

void cnfizeAtMost_K_Parallel(const Constraint &constraint, VariableManager &variableManager, CNF &cnf);
void cnfizeAtMost_K_SequentialCounter_Sinz(const Constraint& constraint,
		VariableManager &variableManager, CNF &cnf);

// returns True if num is prime
bool isPrime(int num)
{
	bool prime = true;
	for(int i = 2; i <= sqrt(num); i++)
	{
		if((int(num)% i) == 0)
		{
			prime = false;
			break;
		}
	}

	return prime;
}
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

// Returns the first m prime numbers larger than R
// If a number is divisible by a non-prime number,
// there is also some prime <= that divisor which it is also divisble by.
void getLargePrimes(vector<int>& primes, unsigned m, unsigned R){
	assert(m>0 && R>0);
	int num = R;
	while(primes.size() != m) {
		while (!isPrime(num)) {
			num++;
		}
		primes.push_back(num);
		num++;
	}
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
// The Hybrid algorithm for creating a CNF for x1 + ... + xn <=k.
// number M of "hash" functions to consider
// lower bound R on the size of prime numbers
void cnfizeAtMost_K_Hybrid(const Constraint& constraint,unsigned M,unsigned R ,VariableManager &variableManager, CNF &cnf){
    assert(constraint.getType() == LEQ_CONSTRAINT);
    
	//1.applying parallel
	cnfizeAtMost_K_Parallel(constraint, variableManager, cnf);

	unsigned k = constraint.getK();
	const std::vector<int>& x = constraint.getLits();
	unsigned n = x.size();

    // There is no point to apply hashing + sequential if R is larger than n
    if (R >= n) {
        return;
    }

    // This is a 1-line command to pass all activation literals
    // to translated constraints
    
	//2.choosing M prime numbers larger than R
	vector<int> primes;
	getLargePrimes(primes,M,R);
	assert(primes.size() == M);
    
	for (std::vector<int>::iterator p_i = primes.begin();
			p_i != primes.end(); ++p_i) {
		unsigned r = *p_i;
		vector<int> y;
		for(unsigned i = 0; i < r ; ++i){
			y.push_back(variableManager.getNewVar());
		}
        // Sasha: Moving this here, as this needs to be set for binary clauses
        cnf.setActivationLits( constraint.getActivationLits() );
		for(unsigned j = 0; j < n ; ++j){
			cnf.addBinaryClause(-x[j], y[j%r]);
		}
		// Y_1 + ... + Y_r <= k using Sequential Counter Encoding.
		cnfizeAtMost_K_SequentialCounter_Sinz( Constraint(LEQ_CONSTRAINT, y,
				constraint.getActivationLits(),k),variableManager,cnf);
	}
	cnf.clearActivationLits();
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
