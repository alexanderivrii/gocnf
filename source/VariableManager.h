#ifndef _VARIABLE_MANAGER_H
#define _VARIABLE_MANAGER_H

// This class handles creation of variables
// The current (very simple) implementation assumes that
//   - setMaxOriginalVar is called once after parsing/reading the original formula
//     (this sets the number of original variables)
//   - getNewVar is used for creating all auxiliary variables
class VariableManager
{
private:
    unsigned maxVar;

public:
    VariableManager() : maxVar(0)
    {
    }
    
    ~VariableManager()
    {
    }
    
    void setMaxOriginalVar(unsigned maxOriginalVar)
    {
        maxVar = maxOriginalVar;
    }
    
    unsigned getNewVar()
    {
        return ++maxVar;
    }
    
    unsigned getMaxVar() const
    {
        return maxVar;
    }
};


#endif
