#include <iostream>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include "CNF.h"
#include "GoCNF.h"

using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;

void cnfizeAtLeastOne(const Constraint &constraint, VariableManager &variableManager, CNF &outCnf);
void cnfizeAtMostOne_Naive (const Constraint &constraint, VariableManager &variableManager, CNF &outCnf);
void cnfizeAtMostOne_Order (const Constraint &constraint, VariableManager &variableManager, CNF &outCnf);

void cnfizeAtMost_K_Naive(const Constraint& constraint, VariableManager &variableManager, CNF &cnf);
void cnfizeAtMost_K_SequentialCounter_Sinz(const Constraint& constraint, VariableManager &variableManager, CNF &cnf);
void cnfizeAtMost_K_Parallel(const Constraint &constraint, VariableManager &variableManager, CNF &cnf);
void cnfizeAtMost_K_PHF(const Constraint& constraint, VariableManager &variableManager, CNF &cnf);
void cnfizeAtMost_K_Hybrid(const Constraint& constraint,unsigned M,unsigned R ,VariableManager &variableManager, CNF &cnf);

// Discuss:
//  Addng cnfization runtimes
//  Adding cnfization statistics


// This is already a clause -- there is nothing to do
void cnfizeAtLeastOne(const Constraint &constraint,
		VariableManager &variableManager, CNF &cnf)
{    
    assert(constraint.getType() == GEQ_CONSTRAINT);
    assert(constraint.getK() == 1);
    cnf.addClause(constraint.getLits());
}


// The direct encoding for x1 +...+xn <=1  
// Idea: introduce new variables y1,...,yn so that yi is true iff at least one of x1, ..., xi is true; 
// and require that x_(i+1) and y_i are not true together for all i=1,...,n-1.
void cnfizeAtMostOne_Order(const Constraint &constraint,
		VariableManager &variableManager, CNF &cnf)
{
    assert(constraint.getType() == LEQ_CONSTRAINT);
    assert(constraint.getK() == 1);
    const std::vector<int>& x = constraint.getLits();
    unsigned n = x.size();
    assert(n>0);
	
    vector<int> y(n);
    for (unsigned i=0; i<n; ++i) {
        y[i] = variableManager.getNewVar();
    }

    for (unsigned i=0; i<n; i++) {
    	cnf.addBinaryClause(-x[i], y[i]);
    }

    for (unsigned i=0; i<n-1; i++) {
        cnf.addBinaryClause(-y[i], y[i+1]);
    }

    for (unsigned i=0; i<n-1; i++) {    
        cnf.addBinaryClause(-x[i+1], -y[i]);
    }
}

bool isTrivialConstraint(const Constraint& constraint){
	if(constraint.getLits().size() == 0){
		return true;
	}
	if(constraint.getLits().size() <= constraint.getK()
			&& constraint.getType() == LEQ_CONSTRAINT){
		return true;
	}
	if(constraint.getK() == 0 &&
			constraint.getType() == GEQ_CONSTRAINT){
		return true;
	}

	return false;
}

void handleLessEqualZero(Constraint& c, VariableManager &variableManager,
		CNF &outCnf){
	assert(c.getK() == 0 && c.getType() == LEQ_CONSTRAINT);
    outCnf.setActivationLits( c.getActivationLits() );        
	for(vector<int>::iterator iter = c.getLits().begin();
			iter != c.getLits().end(); ++iter){
		outCnf.addUnaryClause(-*iter);
	}
    outCnf.clearActivationLits();
}

void GoCNF::cnfizeAllConstraintsNaive(VariableManager &variableManager,
		CNF &inCnf, CNF &outCnf)
{
    for (unsigned i=0; i<inCnf.constraints.size(); ++i)
    {
        Constraint &c = inCnf.constraints[i];
        assert(c.getType() == GEQ_CONSTRAINT ||
        		c.getType() == LEQ_CONSTRAINT);
        if(isTrivialConstraint(c)){
        	continue;
        }

        if(c.getType() == GEQ_CONSTRAINT){
            assert(c.getK() == 1);
            cnfizeAtLeastOne(c, variableManager, outCnf);
        } else {
        	if(c.getK() == 0){
        		handleLessEqualZero(c, variableManager, outCnf);
        		continue;
        	}
			if(c.getK() == 1){
				cnfizeAtMostOne_Naive(c, variableManager, outCnf);
			} else{
				cnfizeAtMost_K_Naive(c, variableManager, outCnf);
			}
        }
    }
}

void GoCNF::cnfizeAllConstraintsSequential(VariableManager &variableManager,
		CNF &inCnf, CNF &outCnf)
{
    for (unsigned i=0; i<inCnf.constraints.size(); ++i)
    {
        Constraint &c = inCnf.constraints[i];
        assert(c.getType() == GEQ_CONSTRAINT || c.getType() == LEQ_CONSTRAINT);
        if(isTrivialConstraint(c)){
        	continue;
        }

        if(c.getType() == GEQ_CONSTRAINT){
        	assert(c.getK() == 1);
            cnfizeAtLeastOne(c, variableManager, outCnf);
        } else {
        	if(c.getK() == 0){
    			handleLessEqualZero(c, variableManager, outCnf);
    			continue;
    		}
        	cnfizeAtMost_K_SequentialCounter_Sinz(c, variableManager, outCnf);
        }
    }
}

void GoCNF::cnfizeAllConstraintsParallel(VariableManager &variableManager,
		CNF &inCnf, CNF &outCnf)
{
    for (unsigned i=0; i<inCnf.constraints.size(); ++i)
    {
        Constraint &c = inCnf.constraints[i];
        assert(c.getType() == GEQ_CONSTRAINT || c.getType() == LEQ_CONSTRAINT);
        if(isTrivialConstraint(c)){
        	continue;
        }
        if(c.getType() == GEQ_CONSTRAINT){
        	assert(c.getK() == 1);
            cnfizeAtLeastOne(c, variableManager, outCnf);
        } else {
        	if(c.getK() == 0){
    			handleLessEqualZero(c, variableManager, outCnf);
    			continue;
    		}
        	cnfizeAtMost_K_Parallel(c, variableManager, outCnf);
        }
    }
}

void GoCNF::cnfizeAllConstraintsPHF(VariableManager &variableManager,
		CNF &inCnf, CNF &outCnf)
{
    for (unsigned i=0; i<inCnf.constraints.size(); ++i)
    {
        Constraint &c = inCnf.constraints[i];
        assert(c.getType() == GEQ_CONSTRAINT || c.getType() == LEQ_CONSTRAINT);
        if(isTrivialConstraint(c)){
        	continue;
        }
        if(c.getType() == GEQ_CONSTRAINT){
        	assert(c.getK() == 1);
            cnfizeAtLeastOne(c, variableManager, outCnf);
        } else {
        	if(c.getK() == 0){
    			handleLessEqualZero(c, variableManager, outCnf);
    			continue;
    		}
        	cnfizeAtMost_K_PHF(c, variableManager, outCnf);
        }
    }
}

void GoCNF::cnfizeAllConstraintsHybrid(VariableManager &variableManager,
		CNF &inCnf, CNF &outCnf)
{
    for (unsigned i=0; i<inCnf.constraints.size(); ++i)
    {
        Constraint &c = inCnf.constraints[i];
        assert(c.getType() == GEQ_CONSTRAINT || c.getType() == LEQ_CONSTRAINT);
        if(isTrivialConstraint(c)){
        	continue;
        }
        if(c.getType() == GEQ_CONSTRAINT){
        	assert(c.getK() == 1);
            cnfizeAtLeastOne(c, variableManager, outCnf);
        } else {
        	if(c.getK() == 0){
    			handleLessEqualZero(c, variableManager, outCnf);
    			continue;
    		}

            cnfizeAtMost_K_Hybrid(c, 1, 2*c.getK(), variableManager, outCnf);
        }
    }
}
