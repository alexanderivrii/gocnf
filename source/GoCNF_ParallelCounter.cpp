// Implements the parallel counter encoding
// Reference: Sinz

#include <iostream>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <stdlib.h>

#include "GoCNF.h"

using std::list;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::prev;


void fullAdder(CNF& cnf, int y, int z, int c_in, int c, int s){
	cnf.addQuaternaryClause(y, z, -c_in, s);
	cnf.addQuaternaryClause(y, -z, c_in, s);

	cnf.addQuaternaryClause(-y, z, c_in, s);
	cnf.addQuaternaryClause(-y, -z, -c_in, s);

	cnf.addTernaryClause(-y, -z, c);
	cnf.addTernaryClause(-y, -c_in, c);
	cnf.addTernaryClause(-z, -c_in, c);
}

void halfAdder(CNF& cnf, int y, int z,  int c, int s){
	cnf.addTernaryClause(y, -z, s);
	cnf.addTernaryClause(-y, -z, c);
	cnf.addTernaryClause(-y, z, s);
}

void printVector(const vector<int>& k){
	int i = 1;
	for (std::vector<int>::const_iterator it = k.begin();
			it != k.end(); ++i, ++it) {
		cout << i << ": " << *it << "  " ;
	}
	cout << endl;
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
vector<int> parallel_aux(VariableManager &variableManager,
		CNF &cnf, const vector<int>& x){

	int n = x.size();

	if(n == 1) {
		return x;
	}

	if(n == 2) {
		vector<int> s;
		s.push_back(variableManager.getNewVar());
		s.push_back(variableManager.getNewVar());
		halfAdder(cnf, x[1], x[0], s[0], s[1]);
		return s;
	}

	vector<int>::const_iterator x_left_begin = x.begin();
	vector<int>::const_iterator x_left_end = x.begin() + n/2;
	vector<int> x_left(x_left_begin, x_left_end);

	assert(x_left.size() > 0);
	vector<int> y = parallel_aux(variableManager, cnf, x_left);

	vector<int>::const_iterator x_right_begin = x.begin() + n/2;
	vector<int>::const_iterator x_right_end = prev(x.end());

	vector<int> x_right(x_right_begin, x_right_end);
	assert(x_right.size() > 0);

	vector<int> z = parallel_aux(variableManager, cnf, x_right);
	vector<int> s, c;
	unsigned fullAdder_num = std::min(z.size(), y.size());


	// we need |c|= fullAdder_num ,
	// |s|=fullAdder_num+1 (we add the last in the end)
	for(unsigned j = 0; j <= fullAdder_num-1; j++) {
		c.push_back(variableManager.getNewVar());
	}
	for(unsigned j = 0; j <= fullAdder_num-1; j++) {
		s.push_back(variableManager.getNewVar());
	}

	int temp_c = x.back();
	int offset = y.size() - z.size();

	for(int i = fullAdder_num-1; i >=  0; --i) {
		fullAdder(cnf, y[i+offset], z[i], temp_c, c[i], s[i]);
		temp_c = c[i];
	}


	if(y.size() > z.size()){
		assert(y.size() == z.size() + 1);
		s.insert(s.begin(), variableManager.getNewVar());
		s.insert(s.begin(), variableManager.getNewVar());
		halfAdder(cnf, y[0], c[0], s[0], s[1]);
	} else {
		s.insert(s.begin(), c[0]);
	}
	return s;
}


/*-----------------------------------------------------------------*/
vector<int> decimalToBinary(int dividend , unsigned bitsNum) {
	list<int> bin_list;
	while (dividend >= 1 ){
	   bin_list.push_front(dividend % 2);
	   dividend /= 2;
	}
	bin_list.insert(bin_list.begin(), bitsNum - bin_list.size(), 0);
	vector<int> res;
	copy(bin_list.begin(),bin_list.end(), back_inserter(res));
	return res;
}

/*-----------------------------------------------------------------*/
list<list<int>> distribute(list<int>& el , list<list<int>> target) {
	for (std::list<list<int>>::iterator iter = target.begin(),
			end = target.end(); iter != end; ++iter) {
		iter->push_front(el.front());
	}
	return target;
}


/*-----------------------------------------------------------------*/
//vector k = Km,Km-1,...,K0  represents the binray value of K
// k.begin() = Km -->MSB  |  K.end() = K0 --> LSB
//vector s = Sm , Sm-1 ...S0   represents the binary value of s
//s.begin()=s0     | s.end() =Sm

list<list<int>> comparatorCircuit(vector<int>& k ,vector<int>& s) {
	list<list<int>> res;
	list<int> first (1,-s[0]);

	s.erase(s.begin());

	if (k.size() == 1) {
		if(k.back() == 0) {
			res.push_back(first);
		}
		k.pop_back();
		return res;
	}

	int k_n = k.front();
	k.erase(k.begin());
	res = comparatorCircuit(k, s);
	if(k_n == 0) {
		res.push_front(first);
		return res;
	}
	//k_n == 1
	if(res.empty()){
		return res;
	}
	return distribute(first, res);
}

/*-----------------------------------------------------------------*/
void convertListsToClauses(	CNF &cnf, list<list<int>>& listOfLists){
	for (std::list<list<int>>::iterator list_iter = listOfLists.begin(),
			end = listOfLists.end(); list_iter != end; ++list_iter) {
		vector<int> lits;
		copy(list_iter->begin(), list_iter->end(), back_inserter(lits));
		cnf.addClause(lits);
	}
}

/*-----------------------------------------------------------------*/
// The parallel encoding for x1 + ... xn <= k
void cnfizeAtMost_K_Parallel(const Constraint &constraint,
		VariableManager &variableManager, CNF &cnf)
{
    assert(constraint.getType() == LEQ_CONSTRAINT);
    const vector<int>& x = constraint.getLits();
    unsigned n = x.size();
    // This is a 1-line command to pass all activation literals to translated constraints
    cnf.setActivationLits( constraint.getActivationLits() );

	vector<int> s = parallel_aux(variableManager, cnf, x);
	unsigned m = static_cast<unsigned>(log2(n));

	vector<int> k = decimalToBinary(constraint.getK(),m+1);
	assert(k.size()==m+1);

	list<list<int>> target = comparatorCircuit(k,s);
	convertListsToClauses(cnf,target);
	cnf.clearActivationLits();
}

