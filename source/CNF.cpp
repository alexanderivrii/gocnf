#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <climits>

#include "CNF.h"

using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::ofstream;
using std::fstream;
using std::string;
using std::stringstream;
using std::getline;
using std::vector;

void CNF::addConstraint(const Constraint& c)
{
    constraints.push_back(c);
    constraints.back().setActivationLits( getActivationLits() );
}

void CNF::addClause(const vector<int>& lits)
{
	addConstraint(Constraint(GEQ_CONSTRAINT, lits, 1));
}

void CNF::addUnaryClause(int lit1)
{
	vector<int> lits; 
	lits.push_back(lit1);
	addClause(lits);
}
void CNF::addBinaryClause(int lit1, int lit2)
{
	vector<int> lits; 
	lits.push_back(lit1);
	lits.push_back(lit2);
	addClause(lits);
}

void CNF::addTernaryClause(int lit1, int lit2, int lit3)
{
	vector<int> lits; 
	lits.push_back(lit3);
	lits.push_back(lit2);
	lits.push_back(lit1);
	addClause(lits);
}

void CNF::addQuaternaryClause(int lit1, int lit2,
		int lit3, int lit4){
	vector<int> lits;
	lits.push_back(lit1);
	lits.push_back(lit2);
	lits.push_back(lit3);
	lits.push_back(lit4);
	addClause(lits);
}

void CNF::printConstraint(const Constraint & c) {
    ConstraintType  t = c.getType();
    const vector<int>& lits = c.getLits();
    const vector<int>& acts = c.getActivationLits();
    unsigned k = c.getK();
    if (acts.size() != 0) {
        for (unsigned j=0; j<acts.size(); ++j) {
            cout << acts[j];
            if (j != acts.size() - 1)
                cout << " & ";
            else 
                cout << " -> ";
        }
    }
    cout << "(";
    for (unsigned j=0; j < lits.size(); ++j) {
        cout << lits[j];
        if(j != lits.size() - 1){
            cout << " ";
        }
    }
    cout << ")";
    if (t == LEQ_CONSTRAINT)
        cout << " <= ";
    else if (t == GEQ_CONSTRAINT)
        cout << " >= ";
    else
    	cout << " = ";
    cout << k << endl;
}

void CNF::print()
{
    cout << "Total number of constraints: " << constraints.size() << endl;
    for (unsigned i = 0; i < constraints.size(); ++i) {
    	printConstraint(constraints[i]);
    }
}
