#ifndef _GOCNF_H
#define _GOCNF_H


#include "VariableManager.h"
#include "CNF.h"
#include <vector>
#include <functional>
#include <string>


namespace GoCNF
{

	bool readFromSAT(std::vector<int>& assignment, const char* filename);
	/*
	 * This function accepts a assignment, where index[i] is the assigned
	 * value for variable with the number i+1, represented by value '1' for true
	 * and '0' for 'false' */
	bool validateAssignment(std::vector<int>& assignment, CNF &cnf);
	/* Adds a clause that exclude the given assignment of literals.
	 * This function accepts a assignment, where index[i] is the assigned
	 * value for variable with the number i+1, represented by value '1' for true
	 * and '0' for 'false'
	 * And a list of literals to be excluded according to the given value by
	 * the given assignment */
	void excludeAssignment(const std::vector<int>& exclude,
			const std::vector<int>& assignment, VariableManager &variableManager, CNF &cnf);

	void readFromFileDIMACS(VariableManager &variableManager, CNF &cnf, const char* const filename);
    void writeToFileDIMACS(VariableManager &variableManager, CNF &cnf, const char* const filename, bool sort = false);
    void readFromFileOPB(VariableManager &variableManager, CNF &cnf, const char* const filename);

    void cnfizeAtMostOne_Order (const Constraint &constraint, VariableManager &variableManager, CNF &outCnf);

	void cnfizeAllConstraintsNaive(VariableManager &variableManager, CNF &inCnf, CNF &outCnf);
	void cnfizeAllConstraintsSequential(VariableManager &variableManager, CNF &inCnf, CNF &outCnf);
	void cnfizeAllConstraintsParallel(VariableManager &variableManager, CNF &inCnf, CNF &outCnf);
	void cnfizeAllConstraintsPHF(VariableManager &variableManager, CNF &inCnf, CNF &outCnf);
	void cnfizeAllConstraintsHybrid(VariableManager &variableManager, CNF &inCnf, CNF &outCnf);
	/*
	 *
	 * When adding new encoding, declare the new
	 * EncodingName enumerate value and EncondingType at the same offset.
	 *
	 * */

	typedef enum {
		NAIVE,
		COUNTER,
		PARALLEL,
		PHF,
		HYBRID
	} EncodingName;

	typedef std::function<void(VariableManager&, CNF& in, CNF& out)> Encoding;

	typedef struct {
		EncodingName name;
		std::string extension;
		Encoding function;
	} EncodingType;

	/*
	 * Order of declaration must be kept.
	 * */
	static const EncodingType encodings[]={
			{NAIVE, "naive", GoCNF::cnfizeAllConstraintsNaive},
			{COUNTER, "counter", GoCNF::cnfizeAllConstraintsSequential},
			{PARALLEL, "parallel", GoCNF::cnfizeAllConstraintsParallel},
			{PHF, "phf", GoCNF::cnfizeAllConstraintsPHF},
			{HYBRID, "hybrid", GoCNF::cnfizeAllConstraintsHybrid}
	};
}


#endif
