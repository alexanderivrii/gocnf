/*
 * GoCNF_PHF.cpp
 *
 *  Created on: May 22, 2016
 *      Author: wardm
 */


#include <assert.h>
#include <cmath>
#include <vector>


#include "GoCNF.h"

using std::vector;

void cnfizeAtMost_K_SequentialCounter_Sinz(const Constraint& constraint,
		VariableManager &variableManager, CNF &cnf);


// Returns the first m primal numbers
// If a number is divisible by a non-prime number,
// there is also some prime <= that divisor which it is also divisble by.
void getPrimes(vector<int>& primes, unsigned m  ){
	assert(m>0);
    primes.push_back(2);
	int i = 3;
	while(primes.size() != m) {
		bool prime = true;
		for(unsigned j = 0; j < primes.size()
				&& primes[j]*primes[j] <= i; j++){
			if(i % primes[j] == 0){
				prime = false;
				break;
			}
		}
		if(prime) {
			primes.push_back(i);
		}
		i++;
	}
}

// The PHF algorithm for creating a CNF for x1 + ... + xn <=k.
void cnfizeAtMost_K_PHF(const Constraint& constraint, VariableManager &variableManager, CNF &cnf){
    assert(constraint.getType() == LEQ_CONSTRAINT);
    // This is a 1-line command to pass all activation literals
    // to translated constraints
    cnf.setActivationLits( constraint.getActivationLits() );

	unsigned k = constraint.getK();
	const std::vector<int>& x = constraint.getLits();
	unsigned n = x.size();
	unsigned q = k+1;

	vector<int> primes;
	getPrimes(primes,(int)(q*q*(log2(n))) +1);
	assert(primes.size() >= q*q*log2(n));

	for (std::vector<int>::iterator p_i = primes.begin();
			p_i != primes.end(); ++p_i) {
		unsigned r = *p_i;
		vector<int> y;
		for(unsigned i = 0; i < r ; ++i){
			y.push_back(variableManager.getNewVar());
		}

		for(unsigned j = 0; j < n ; ++j){
			cnf.addBinaryClause(-x[j], y[j%(*p_i)]);
		}
		// Y_1 + ... + Y_r <= k using Sequential Counter Encoding.
		cnfizeAtMost_K_SequentialCounter_Sinz( Constraint(LEQ_CONSTRAINT, y,
				constraint.getActivationLits(),k),variableManager,cnf);
	}
	cnf.clearActivationLits();
}


