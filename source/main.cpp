#include <iostream>
#include <time.h>
#include <assert.h>
#include <vector>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <functional>

#include "GoCNF.h"

using std::string;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::ofstream;
using std::fstream;
using std::string;
using std::stringstream;
using std::getline;
using std::vector;

using GoCNF::EncodingName;
using GoCNF::EncodingType;
using GoCNF::Encoding;
using GoCNF::NAIVE;
using GoCNF::COUNTER;
using GoCNF::PARALLEL;
using GoCNF::PHF;
using GoCNF::HYBRID;

// structure that holds all the implemented encodings
using GoCNF::encodings;

// this needs to be updated each time a new encoding is added
bool isValidEncoding(EncodingName);


/*
 * CURRENTLY SUPPORTED USAGE:
 *   rungocnf
 *      - runs testing
 *   rungocnf <input_opb_file> <output_file_dimacs> <method>
 *      - parses input_opb_file, cnfizes, and writes output to output_file_dimacs
 *      - method is one of {1, 2, 3, 4, 5} = { naive, seq, par, phf, hyb }
 *        WE NEED TO MAKE THIS MORE GENERIC, SOMEHOW
*/


// functions used to present the project
void runExample(const char*);
void showPresentation(void);

// functions to help with testing
bool openFile(const char *filename, fstream& input);
void runTesting(bool silent);
void printVector(const vector<int>& k);
void runMiniSAT(string outName, string testName, CNF cnf);
void runTest(bool silent, string testName, EncodingType encoding);
void runTestMiniSAT(bool silent, string outName,
		string testName, EncodingType encoding, CNF cnf);
void runAutoTests(bool silent);
void runAutoTest(bool silent, string in_name, string exp_name,
		EncodingType encoding);
void runAutoTestMiniSAT(bool silent, string outName,
		string testName, string exp_name,
		EncodingType encoding, CNF cnf);
bool vectorsEqual(vector<int>& v1, vector<int>& v2);
void printInstructions();


// path for miniSAT program on current machine
// change accordingly
#define MINISAT "../../minisat/minisat_static"

// defines to help with testing
#define MINISAT_RESULT_DIR "../testing/minisat_results/"
#define MINISAT_RESULT_AUTO_DIR "../testing/minisat_results_auto/"

#define GO_CNF_INPUT "../testing/in/"
#define GO_CNF_AUTO_INPUT "../testing/generate_constraints/in/"
#define GO_CNF_AUTO_EXPECTED "../testing/generate_constraints/out/"

#define GO_CNF_OUTPUT "../testing/out/"
#define GO_CNF_AUTO_OUTPUT "../testing/out_auto/"

void printInstructions(){
	cout << "usage: GoCNF <input-file> [ <output-file>  <encoding-number> [ <run-miniSAT> ]] " << endl;
	cout << "<input-file> OPB file to read input from" << endl;
	cout << "Optional: <output-file> file to write result into " << endl;
	cout << "	  and <encoding-number>, which encoding" << endl;
	cout << "	  to use, if no encoding chosen we do not translate" << endl;
	cout << " 0 - Naive Encoding" << endl;
	cout << " 1 - Sequential Counter (Sinz) Encoding" << endl;
	cout << " 2 - Parallel Counter Encoding" << endl;
	cout << " 3 - PHF Encoding" << endl;
	cout << " 4 - Hybrid Encoding" << endl;
	cout << "Optional: <run-miniSAT>" << endl;
	cout << "	  if <output-file> given and <run-miniSAT> is set to 1" << endl;
	cout << "	  runs result on miniSAT and writes the answer to a file" << endl;
	cout << "	  with the same name prefixed with 'miniSAT_'" << endl;
}


int main(int argc, char ** argv){
    if(argc == 1) {
    	printInstructions();
    	// Presenting Go_CNF Project
    	//showPresentation();
    	return 0;
    } else if (argc >= 2) {
        // Sasha: I like to be able to load opb file without having to translate it
        //  or to output the result into a file
		const char* opbReadFileName = NULL;
		const char* dimacsWriteFileName = NULL;
		int    encodingMethod = -1;
		opbReadFileName = argv[1];
		bool runMiniSAT = false ;

        if (argc >= 3) dimacsWriteFileName = argv[2];
        if (argc >= 4) encodingMethod = atoi(argv[3]);
        if (argc >= 5) runMiniSAT = (bool) atoi(argv[4]);

		VariableManager vM;
		CNF inCnf, outCnf;
		GoCNF::readFromFileOPB(vM, inCnf, opbReadFileName);
		unsigned originalVars = vM.getMaxVar();

        EncodingName encodingName = static_cast<EncodingName>(encodingMethod);

        // Note: if no encoding method is specified, we do not do CNFize
        if (encodingMethod != -1
        		&& isValidEncoding(encodingName)) {
            Encoding encoding = GoCNF::encodings[encodingName].function;
            // translation start time saved
            time_t startTranslation = time(NULL);
            // running the translation
            encoding(vM, inCnf, outCnf);
            // translation end time saved
            time_t endTranslation = time(NULL);

            // saving output to file
            if (dimacsWriteFileName != NULL) {
            	GoCNF::writeToFileDIMACS(vM,
            			outCnf, dimacsWriteFileName);
            }

            // Sasha:
            // (1) I get complication errors for to_string(time_t)
            // (2) It is much more standard to print the number of seconds anyways
            //     (including 0 to mean less than a second:)
            
            cout << "Translation [" << GoCNF::encodings[encodingName].extension  << "]: aux vars = " 
                << vM.getMaxVar() - originalVars 
                << ", total vars = " << vM.getMaxVar() 
                << ", total clauses = " << outCnf.constraints.size() 
                << ", translation time (seconds) = " <<
					difftime(endTranslation, startTranslation) << endl;

            // if we choose to run miniSAT on the output
            if(runMiniSAT &&
            	dimacsWriteFileName != NULL){
            	// printing output
            	string fullPath(dimacsWriteFileName);
            	string relative(fullPath.substr(0,
            			fullPath.find_last_of("/") == string::npos ? 0 : fullPath.find_last_of("/")+1));
            	if(relative.empty()){
            		relative = string("./");
            	}
            	string fileName(fullPath.substr(fullPath.find_last_of("/")
            			== string::npos ? 0 : fullPath.find_last_of("/")+1));
            	string newName("miniSAT_");
            	newName += fileName;
            	// MINISAT is the full path to miniSAT on machine
            	string command(MINISAT);
            	command += " ";
            	command += dimacsWriteFileName;
            	command += " ";
            	command += relative;
            	command += newName;
            	// running miniSAT
            	std::system(command.c_str());

            	cout << "miniSAT answer written to " << relative
            			<< newName << endl;
            }
        }
    } else {
		cout << "Incorrect argument list" << endl;
		printInstructions();
	}
	return 0;
}

bool isValidEncoding(EncodingName encoding){
	return encoding >= NAIVE && encoding <= HYBRID;
}





// functions for testing
void runAutoTests(bool silent){
	std::system("mkdir -p ../testing/minisat_results_auto");
	std::system("mkdir -p ../testing/out_auto");

	fstream in_names;
	if(!openFile("../../testing/generate_constraints/in/in_files", in_names)){
		return;
	}

	fstream out_names;
	if(!openFile("../../testing/generate_constraints/out/out_files", out_names)){
		in_names.close();
		return;
	}
	string in_line, out_line;
	while(!in_names.eof() && !out_names.eof()){
		getline(in_names, in_line);
		if(in_line.empty()){
			continue;
		}
		getline(out_names, out_line);
		if(out_line.empty()){
			continue;
		}
		runAutoTest(silent, in_line, out_line, encodings[COUNTER]);
		runAutoTest(silent, in_line, out_line, encodings[PARALLEL]);
		runAutoTest(silent, in_line, out_line, encodings[PHF]);
		runAutoTest(silent, in_line, out_line, encodings[HYBRID]);
	}
	in_names.close();
	out_names.close();
}

// perform unit testing
void runTesting(bool silent) {
	runTest(silent, "inter1", encodings[HYBRID]);
	runTest(silent, "in1", encodings[NAIVE]);
	runTest(silent, "in1", encodings[COUNTER]);
	runTest(silent, "in1", encodings[PARALLEL]);
	runTest(silent, "in1", encodings[PHF]);

	runTest(silent, "in2", encodings[COUNTER]);
	runTest(silent, "in2", encodings[PARALLEL]);
	runTest(silent, "in2", encodings[PHF]);

	runTest(silent, "queens_3", encodings[NAIVE]);

	runTest(silent, "queens_3", encodings[COUNTER]);
	runTest(silent, "queens_3", encodings[PARALLEL]);
	runTest(silent, "queens_3", encodings[PHF]);

	runTest(silent, "queens_4", encodings[NAIVE]);
	runTest(silent, "queens_4", encodings[COUNTER]);
	runTest(silent, "queens_4", encodings[PARALLEL]);
	runTest(silent, "queens_4", encodings[PHF]);
}


bool vectorsEqual(vector<int>& v1, vector<int>& v2){
	if(v1.size() != v2.size()){
		return false;
	}
	for(vector<int>::iterator i = v1.begin(), j = v2.begin();
			i != v1.end() && j != v2.end(); ++i, ++j){
		if(*i != *j){
			return false;
		}
	}
	return true;
}

bool vectorsEqualUpTo(vector<int>& v1, vector<int>& v2, unsigned n = 0){
	if(n == 0){
		if(v1.size() != v2.size()){
			return false;
		}
	} else {
		if(v1.size() < n || v2.size() < n){
			return false;
		}
	}
	unsigned upTo = n == 0 ? 0 : 1;
	for(vector<int>::iterator i = v1.begin(), j = v2.begin();
			i != v1.end() && j != v2.end() && upTo <= n;
			++upTo, ++i, ++j){
		if(*i != *j){
			return false;
		}
	}
	return true;
}


void runAutoTest(bool silent, string in_name, string exp_name,
		EncodingType encoding){
	VariableManager vM;
	CNF inCnf, outCnf;

	// getting input
	GoCNF::readFromFileOPB(vM, inCnf, in_name.c_str());
	if(!silent){
		cout << "We input " << in_name.substr(in_name.find_last_of("/"))
				<< " to the " << encoding.extension << " encoding: " << endl;
		inCnf.print();
		cout << endl;
	}

	// encoding to CNF
   	encoding.function(vM, inCnf, outCnf);

	// writing output
	string outName(GO_CNF_AUTO_OUTPUT);
	outName += "CNF_";
	outName += in_name.substr(in_name.find_last_of("/")+1);
	outName += "_";
	outName += encoding.extension;
	GoCNF::writeToFileDIMACS(vM, outCnf, outName.c_str());
	if(!silent){
		cout << "We get this CNF: " << endl;
		outCnf.print();
		cout << "-----------------" << endl;
	}
	runAutoTestMiniSAT(silent, outName,
			in_name.substr(in_name.find_last_of("/")+1),
			exp_name, encoding, outCnf);
}

void runAutoTestMiniSAT(bool silent, string outName,
		string testName, string exp_name,
		EncodingType encoding, CNF cnf){
	string command(MINISAT);
	command += " ";
	if(silent){
		command += " -verb=0 ";
	}

	command += outName;
	command += " ";
	outName = MINISAT_RESULT_AUTO_DIR;
	outName += "CNF_";
	outName += testName;
	outName += "_";
	outName += encoding.extension;
	outName += "_minisat_result";
	command += outName;

	// run miniSAT
	system(command.c_str());

	if(!silent){
		cout << endl;
		cout << "The Result is:"  << endl;
		cout << endl;
	}

	// printing miniSAT result
	command = "cat ";
	command += outName;
	system(command.c_str());
	cout << endl;
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	cout << endl;

	bool exp = true;
	vector<int> outputValues;
	if(GoCNF::readFromSAT(outputValues, outName.c_str())){
		if(!silent){
			cout << "Assignment is: " << endl;
			printVector(outputValues);
		}
		if(GoCNF::validateAssignment(outputValues, cnf)){
			if(!silent){
				cout << "Assignment is valid" << endl;
			}
		} else {
			cout << "Assignment is not valid" << endl;
			exit(-1);
		}
	} else {
		exp = false;
	}

	vector<int> expectedValues;
	if(GoCNF::readFromSAT(expectedValues, exp_name.c_str())){
		if(!exp) {
			cout << "Expected SAT, got UNSAT" << endl;
			exit(-1);
		}
	} else {
		if(exp) {
			cout << "Expected UNSAT, got SAT" << endl;
			exit(-1);
		}
	}

	if(vectorsEqualUpTo(expectedValues, outputValues, expectedValues.size())){
		if(!silent){
			cout << "Assignments are equals" << endl;
		}
	} else {
		cout << "Assignments are not equals" << endl;
		exit(-1);
	}
}


void runTestMiniSAT(bool silent, string outName,
		string testName, EncodingType encoding, CNF cnf){
	string command(MINISAT);
	command += " ";
	if(silent){
		command += " -verb=0 ";
	}
	command += outName;
	command += " ";
	outName = MINISAT_RESULT_DIR;
	outName += "CNF_";
	outName += testName;
	outName += "_";
	outName += encoding.extension;
	outName += "_minisat_result";
	command += outName;

	// run miniSAT
	system(command.c_str());

	if(!silent){
		cout << endl;
		cout << "The Result is:"  << endl;
		cout << endl;
	}

	// printing miniSAT result
	command = "cat ";
	command += outName;
	system(command.c_str());
	cout << endl;
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	cout << endl;

	vector<int> values;

	if(GoCNF::readFromSAT(values, outName.c_str())){
		if(!silent){
			cout << "Assignment is: " << endl;
			printVector(values);
		}
		if(GoCNF::validateAssignment(values, cnf)){
			if(!silent){
				cout << "Assignment is valid" << endl;
			}
		} else {
			cout << "Assignment is not valid" << endl;
			exit(-1);
		}
	} else {
		if(GoCNF::validateAssignment(values, cnf)){
			if(!silent){
				cout << "Assignment is valid and it should't be" << endl;
				exit(-1);
			}
		} else {
			if(!silent){
				cout << "Assignment is not valid" << endl;
			}
		}
	}
}


void runTest(bool silent, string testName, EncodingType encoding){
	VariableManager vM;
	CNF inCnf, outCnf;

	// getting input
	string inputName(GO_CNF_INPUT);
	inputName += testName;
	GoCNF::readFromFileOPB(vM, inCnf, inputName.c_str());
/*	if(!silent){
		cout << "We input " << testName << " to the "
				<< encoding.extension << " encoding: " << endl;
		inCnf.print();
		cout << endl;
	}
*/
	// encoding to CNF
	encoding.function(vM, inCnf, outCnf);

	// writing output
	string outName(GO_CNF_OUTPUT);
	outName += "CNF_";
	outName += testName;
	outName += "_";
	outName += encoding.extension;
	GoCNF::writeToFileDIMACS(vM, outCnf, outName.c_str());
	if(!silent){
		cout << "We get this CNF: " << endl;
		outCnf.print();
		cout << "-----------------" << endl;
	}
	// running miniSAT on output and printing result
	runTestMiniSAT(silent, outName, testName, encoding, outCnf);
}


void runMiniSAT(string outName, string testName, CNF cnf){
	string command(MINISAT);
	command += " ";
	command += outName;
	command += " ";
	outName = MINISAT_RESULT_DIR;
	outName += testName;
	outName += "_minisat_result";
	command += outName;

	// run miniSAT
	system(command.c_str());

	cout << endl;
	cout << "The Result is:"  << endl;
	cout << endl;

	// printing miniSAT result
	command = "cat ";
	command += outName;
	system(command.c_str());
	cout << endl;
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	cout << endl;
}


// functions for demonstration and testing
void clear(){
	for(int i = 0; i < 100; i++){
		cout << endl;
	}
}
void runExample(const char* test){
	cout << "Running example 1 - simple constraint on 5 variables" << endl;
	std::system("cat ../testing/in/simple");
	getchar();
	cout << "1) using Sequential Counter (Sinz) Encoding" << endl;
	getchar();
	runTest(false, test, encodings[COUNTER]);
	getchar();
	clear();
	cout << "2) using Parallel Counter Encoding" << endl;
	getchar();
	runTest(false, test, encodings[PARALLEL]);
	getchar();
	clear();
	cout << "3) using PHF Encoding" << endl;
	getchar();
	runTest(false, test, encodings[PHF]);
	getchar();
	clear();
	cout << "4) using Hybrid Encoding" << endl;
	getchar();
	runTest(false, test, encodings[HYBRID]);
	getchar();
	clear();
}

void showPresentation(void){
	getchar();
	clear();
	cout << "---------------------------------------------------" << endl;
	runExample("simple");
	clear();
	cout << "---------------------------------------------------" << endl;
	cout << "Running example 2 - complex example: Queens Problem 5x5" << endl;
	getchar();
	std::system("cat /home/wardm/Documents/gocnf/testing/in/queens_5");
	getchar();
	cout << "using PHF Encoding" << endl;
	getchar();
	runTest(false, "queens_5", encodings[PHF]);
	getchar();
	clear();
	cout << "using Hybrid Encoding" << endl;
	getchar();
	runTest(false, "queens_5", encodings[HYBRID]);
	getchar();
	getchar();
	clear();
	std::system("cat /home/wardm/Documents/gocnf/testing/like");
}
