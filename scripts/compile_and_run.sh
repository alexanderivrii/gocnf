#!/bin/bash
TEST=`./compile.sh 2>&1`
ERR_NUM=`echo $TEST | wc -c`
if [[ $ERR_NUM -eq 1 ]]; 
then
	./run
else
	if [[ $1 == "show" ]]; then
		echo "$TEST"
	else 
		echo "compiling error"
	fi
fi
