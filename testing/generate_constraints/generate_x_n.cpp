#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

typedef std::vector<int> int_v;
typedef std::vector<int>::iterator iter_v;

void print_instructions();
bool openToWriteFile(const char *filename, std::fstream& output);
void make_test_input(std::fstream& testInFiles, int_v& vars, int count, 
		int k, int sum, int option);
void make_test_output(std::fstream& testOutFiles, int_v& vars, int count, 
		int k, int sum, int option);
void fill_and_print(std::fstream& in_names, std::fstream& out_names, 
		int_v& vars, int n, int sum, int k, int option);

int count = -1;
int max = -1;
int coerce = 1;

int main(int argc, char* argv[]){
	if(argc != 3 && argc != 4 && argc != 5 && argc != 6){
		print_instructions(); 
		return -1;
	}
	int var_n = atoi(argv[1]);
	int k = atoi(argv[2]);
	if(var_n <= 0){
		print_instructions();
		std::cout << " * Error: #variables > 0" << std::endl;
		return -1;
	}
	if(k < 0){
		print_instructions();
		std::cout << " * Error: MAX k >= 0" << std::endl;
		return -1;
	}
	int option = 0;
	if(argc >= 4){
		option = atoi(argv[3]);
		if(option != 0 && option != 1 && option != 2){
			print_instructions();
			std::cout << " * Error: option 0 for LEQ (default), 1 for GEQ \
and 2 for both" << std::endl;
			return -1;	
		}
	}

	if(argc >= 5){
		max = atoi(argv[4]);
		if(max <= 0){
			print_instructions();
			std::cout << " * Error: MAX #constraints > 0" << std::endl;
			return -1;	
		}
		
	}

	count = 0;

	if(argc >= 6){
		coerce = atoi(argv[5]);
		if(coerce < 0 || coerce > 1){
			print_instructions();
			std::cout << " * Error: coerce should be 0 or 1" << std::endl;
			return -1;	
		}
	}


	std::system("rm -rf in");
	std::system("rm -rf out");

	std::system("mkdir -p in");
	std::system("mkdir -p out");

	std::fstream in_names, out_names;

	if(!openToWriteFile("./in/in_files", in_names)){
		print_instructions();
		std::cout << " * cannot create ./in/in_files for writing input names" << std::endl;
		return -1;
	}
	if(!openToWriteFile("./out/out_files", out_names)){
		print_instructions();
		std::cout << " * cannot create ./out/out_files for writing output names" << std::endl;
		in_names.close();
		return -1;
	}

	int_v x(var_n, 0);
	if(option == 2){
		for (int op = 0; op < option; op++)
			for(int i = 0; i <= k; i++){
				if(max != -1 && count == max){
					goto finish;
				}
				fill_and_print(in_names, out_names, x, 0, 0, i, op);	
			}
		goto finish;
	}
	for(int i = 0; i <= k; i++){
		if(max != -1 && count == max){
			break;
		}
		fill_and_print(in_names, out_names, x, 0, 0, i, option);	
	}
finish:	
	std::cout << " * made " << count << " constraints on " << var_n;
	std::cout << " vars with ";
	std::cout << (option > 0 ? (option > 1 ? "<= and >=" : ">=") : "<=");
	std::cout << " sign" << " and k range of [0, " << k << "]" << std::endl;	
	if(coerce){
		std::cout << " * Values were coerced" << std::endl;
		std::cout << " * Input saved to ./in/" << std::endl;	
		std::cout << " * Input names in ./in/in_files and output names correspondingly, in ./out/out_files " << std::endl;
		std::cout << " * Output saved to ./out/" << std::endl;
	} else {
		std::cout << " * Input saved to ./in/" << std::endl;	
		std::cout << " * Input names in ./in/in_files" << std::endl;
	}
	in_names.close(), out_names.close();
	return 0;
}

void print_instructions(){
	std::cout << " * Use generate_x_n \
[#variables] [MAX k] [optional: 0 for LEQ (default), 1 for GEQ and 2 for both\
 [optional: MAX #constraints [optional: coerce values? 0 for No, 1 for Yes (default is 1)]]]" << std::endl;
	std::cout << " * Input saved to ./in/" << std::endl;
	std::cout << " * Input names in ./in/in_files and output names correspondingly, in ./out/out_files if coerce values is set to 1" << std::endl;
	std::cout << " * Output saved to ./out/" << std::endl;
		
}

bool openToWriteFile(const char *filename, std::fstream& output){
	if(!filename){
		std::cerr << " * Error - Empty string" << std::endl;
		return false;
	}
	output.open(filename, std::fstream::trunc | std::fstream::out);
	if(!output.is_open()){
		std::cerr << " * Error - Could not create " << filename
				<< " for writing output" << std::endl;
		return false;
	}
	return true;
}

void make_test_input(std::fstream& in_names, int_v& vars, int count, 
		int k, int sum, int option){
	std::string in_name("./in/in_");
	in_name += std::to_string(vars.size());
	in_name += "_";
	in_name += std::to_string(sum);
	in_name += "_";
	in_name += ((option == 0) ? "LEQ" : "GEQ");
	in_name += "_";
	in_name += std::to_string(k);
	in_name += "_";
	in_name += std::to_string(count);
	std::fstream input_file;
	if(!openToWriteFile(in_name.c_str(), input_file)){
		std::cerr << " * Error - Could not create " << in_name
			<< " for writing input" << std::endl;			
		return;
	}
	std::string fullPath("/home/wardm/Documents/gocnf/testing/generate_constraints");
	fullPath += in_name.substr(1, in_name.length()-1);
	in_names << fullPath.c_str() << std::endl;
	if(coerce){
		for(int i = 1; i <= (int) vars.size(); i++){
			input_file << "+1 " << ((vars[i-1] == 0) ? "~" : "");
			input_file << "x" << i << " >= 1;" << std::endl;  
		}
	}
	for(int i = 1; i <= (int) vars.size(); i++){
		input_file << "+1 x" << i << " ";  
	}
	input_file << ((option == 0) ? "<=" : ">=") << 
		" " << k << ";" << std::endl;
	input_file.close();
}

void make_test_output(std::fstream& out_names, int_v& vars, int count, 
		int k, int sum, int option){
	std::string exp_out_name("./out/exp_out_");
	exp_out_name+= std::to_string(vars.size());
	exp_out_name += "_";
	exp_out_name += std::to_string(sum);
	exp_out_name += "_";
	exp_out_name += ((option == 0) ? "LEQ" : "GEQ");
	exp_out_name += "_";
	exp_out_name += std::to_string(k);
	exp_out_name += "_";
	exp_out_name += std::to_string(count);
	std::fstream output_file;
	if(!openToWriteFile(exp_out_name.c_str(), output_file)){
		std::cerr << " * Error - Could not create " << exp_out_name
			<< " for writing expected output" << std::endl;			
		return;
	}
	std::string fullPath("/home/wardm/Documents/gocnf/testing/generate_constraints");
	fullPath += exp_out_name.substr(1, exp_out_name.length()-1);
	out_names << fullPath.c_str() << std::endl;
	if((option == 0 && sum > k) || 
		(option == 1 && sum < k)){
		output_file << "UNSAT";
	} else {
		output_file << "SAT" << std::endl;
		for(int i = 1; i <= (int) vars.size(); i++){
			output_file << ((vars[i-1] == 0) ? -i : i);
			output_file << " ";  
		}	
		output_file << "0" << std::endl;	
	}
	output_file.close();
}

void fill_and_print(std::fstream& in_names, std::fstream& out_names,
		int_v& vars, int n, int sum, int k, int option){
	if(max != -1 && count == max){
		return;
	}

	if((unsigned) n == vars.size()){
		count++;
		make_test_input(in_names, vars, count, k, sum, option);
		if(coerce) make_test_output(out_names, vars, count, k, sum, option);
		return;
	}

	vars[n] = 0;
	fill_and_print(in_names, out_names, vars, n + 1, sum, k, option);
	vars[n] = 1;
	fill_and_print(in_names, out_names, vars, n + 1, sum + 1, k, option);
}

