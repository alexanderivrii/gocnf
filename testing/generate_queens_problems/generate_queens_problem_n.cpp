#include <iostream>

int main(void){
#ifndef n
	std::cout << "Error: n not defined, please define n > 2 using -Dn=" << std::endl;
	return -1;
#elif (n <= 2)
	std::cout << "Error: n out of range, please define n > 2 using -Dn=" << std::endl;
	return -1;
#else 
	int variables[n][n];
	int variables_index = 1;	
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			variables[i][j] = variables_index++;
		}
	}
	std::cout << "* testing Queens Problem for n=" << n << std::endl;
	std::cout << "* " << std::endl;
	std::cout << "* Rules for rows and coloumns " << std::endl;
	for(int i = 1; i <= n; i++){
		for(int j = 1; j <= n; j++){
			std::cout << "+1 x" << variables[i-1][j-1] << " "; 
		}	
		std::cout << ">= 1;";
		std::cout << std::endl;
		for(int j = 1; j <= n; j++){
			std::cout << "+1 x" << variables[i-1][j-1] << " "; 
		}	
		std::cout << "<= 1;";
		std::cout << std::endl;
	}
	std::cout << "* " << std::endl;
	for(int j = 1; j <= n; j++){
		for(int i = 1; i <= n; i++){
			std::cout << "+1 x" << variables[i-1][j-1] << " "; 
		}	
		std::cout << ">= 1;";
		std::cout << std::endl;
		for(int i = 1; i <= n; i++){
			std::cout << "+1 x" << variables[i-1][j-1] << " "; 
		}	
		std::cout << "<= 1;";
		std::cout << std::endl;
	}
	std::cout << "* " << std::endl;
	std::cout << "* Rules for diagnal and antidiagnal" << std::endl;
	for(int iter = 3; iter <= 2*n - 1; iter++){
		for(int i = 1; i <= n; i++){
			if(i >= iter){ 
				break;
			}
			for(int j = 1; j <= n; j++){
				if(i + j > iter){ 
					break;
				}	
				if(i + j == iter){
					std::cout << "+1 x" << variables[i-1][j-1] << " ";
				}
			}
		}
		std::cout << "<= 1;";
		std::cout << std::endl;
	}
	std::cout  << "* " << std::endl;

	for(int iter = -n+2; iter <= n-2; iter++){
		for(int i = 1; i <= n; i++){
			if((i - 1) < iter){
				continue;
			}
			if((i - n) > iter){
				break;
			}
			for(int j = 1; j <= n; j++){
				if(i - j < iter){ 
					break;
				}	
				if(i - j == iter){
					std::cout << "+1 x" << variables[i-1][j-1] << " ";
				}
			}
		}
		std::cout << "<= 1;";
		std::cout << std::endl;
	}
#endif
}
